from django.apps import AppConfig


class PgtocsvConfig(AppConfig):
    name = 'pgtocsv'
