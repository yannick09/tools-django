import csv

from django.core.management.base import BaseCommand
from django.db import connection


class Command(BaseCommand):
    help = 'Download data from db and fetch it to a CSV file'

    def handle(self, *args, **kwargs):
        QUERY = 'SELECT * FROM record'
        OUTPUT_CSV_FILE_NAME = 'db_records.csv'
        FETCH_ITER_SIZE = 100000

        self.stdout.write("Successfully connected to database...\n")
        cursor = connection.cursor()

        self.stdout.write(f"Executing Query `{QUERY}`...")
        cursor.execute(QUERY)
        self.stdout.write("Query executed!!...\n")

        columns_names = [column.name for column in cursor.description]
        self.stdout.write(f"Columns: {columns_names}\n")

        self.stdout.write(f"Opening {OUTPUT_CSV_FILE_NAME} for copy...")
        with open(OUTPUT_CSV_FILE_NAME, 'w') as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerow(columns_names)
            self.stdout.write("Csv file updated with the columns in header...")

            is_fetching_completed = False
            while not is_fetching_completed:
                self.stdout.write("Fetching data...")
                records = cursor.fetchmany(size=FETCH_ITER_SIZE)
                self.stdout.write(f"{len(records)} fetched from database...")
                csv_writer.writerows(records)
                self.stdout.write("Csv file updated...")
                is_fetching_completed = len(records) < FETCH_ITER_SIZE

        self.stdout.write("\nDatabase records successfully imported to csv!!")

        cursor.close()

    # def handle_v2(self, *args, **kwargs):
    #     # do the same thing as above with django ORM, this version does not support joins
    #     from pgtocsv.models import Record
    #     OUTPUT_CSV_FILE_NAME = 'db_records.csv'
    #     FETCH_ITER_SIZE = 100000
    #
    #     field_names = [field.name for field in Record._meta.fields]
    #     self.stdout.write(f"Columns: {field_names}\n")
    #
    #     self.stdout.write(f"Opening {OUTPUT_CSV_FILE_NAME} for copy...")
    #     with open(OUTPUT_CSV_FILE_NAME, 'w') as csv_file:
    #         csv_writer = csv.writer(csv_file)
    #         csv_writer.writerow(field_names)
    #         self.stdout.write("Csv file updated with the columns in header...")
    #
    #         i = 0
    #         for record in Record.objects.iterator(chunk_size=FETCH_ITER_SIZE):
    #             values = [str(getattr(record, field_name)) for field_name in field_names]
    #             csv_writer.writerow(values)
    #             i += 1
    #             if i % 20000 == 0:
    #                 self.stdout.write(f"{i} rows...")
    #
    #         self.stdout.write(f"{i} rows...")
    #
    #     self.stdout.write("\nDatabase records successfully imported to csv!!")
