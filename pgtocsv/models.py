# This is an auto-generated Django model module.
# * python manage.py inspectdb > models.py

from django.db import models


class Record(models.Model):
    region = models.TextField(blank=True, null=True)
    country = models.TextField(blank=True, null=True)
    itemtype = models.TextField(blank=True, null=True)
    saleschannel = models.TextField(blank=True, null=True)
    orderpriority = models.TextField(blank=True, null=True)
    orderdate = models.TextField(blank=True, null=True)
    orderid = models.TextField(blank=True, null=True)
    shipdate = models.TextField(blank=True, null=True)
    unitssold = models.TextField(blank=True, null=True)
    unitprice = models.TextField(blank=True, null=True)
    unitcost = models.TextField(blank=True, null=True)
    totalrevenue = models.TextField(blank=True, null=True)
    totalcost = models.TextField(blank=True, null=True)
    totalprofit = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'record'
